# Utils Command

Une commande linux pratique pour les étudiants en informatique

Utilisation : "-i" pour installer des logiciels sympa
              "-d dossier" pour trouver des doublons dans un dossier
              "-m pour mettre totalement à jour Linux
              "-te phrase " pour traduire quelque chose fr -> en
              "-tf phrase " pour traduire quelque chose en -> fr
              "-os" pour voir les infos de son systeme
              "-sc" pour une recherche google directe
              "-sp" pour une recherche google directe private
              "-tp" pour lancer un nouveau tp
              "-c message" pour commit&push directement
              "-plan" pour afficher le planning de la semaine
              "-e message" pour afficher un message de façon stylé
              "-plan " pour afficher l'EDT
